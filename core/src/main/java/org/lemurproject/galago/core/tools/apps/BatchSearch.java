// BSD License (http://lemurproject.org/galago-license)
package org.lemurproject.galago.core.tools.apps;

import org.lemurproject.galago.core.index.IndexPartReader;
import org.lemurproject.galago.core.index.KeyIterator;
import org.lemurproject.galago.core.index.disk.DiskIndex;
import org.lemurproject.galago.core.index.stats.NodeStatistics;
import org.lemurproject.galago.core.parse.stem.Stemmer;
import org.lemurproject.galago.core.retrieval.LocalRetrieval;
import org.lemurproject.galago.core.retrieval.Retrieval;
import org.lemurproject.galago.core.retrieval.RetrievalFactory;
import org.lemurproject.galago.core.retrieval.ScoredDocument;
import org.lemurproject.galago.core.retrieval.iterator.BaseIterator;
import org.lemurproject.galago.core.retrieval.iterator.ExtentIterator;
import org.lemurproject.galago.core.retrieval.iterator.NullExtentIterator;
import org.lemurproject.galago.core.retrieval.iterator.disk.DiskExtentIterator;
import org.lemurproject.galago.core.retrieval.processing.PassageScoringContext;
import org.lemurproject.galago.core.retrieval.processing.ProcessingModel;
import org.lemurproject.galago.core.retrieval.processing.ScoringContext;
import org.lemurproject.galago.core.retrieval.query.Node;
import org.lemurproject.galago.core.retrieval.query.NodeParameters;
import org.lemurproject.galago.core.retrieval.query.StructuredQuery;
import org.lemurproject.galago.core.util.ExtentArray;
import org.lemurproject.galago.utility.ByteUtil;
import org.lemurproject.galago.utility.FixedSizeMinHeap;
import org.lemurproject.galago.utility.queries.JSONQueryFormat;
import org.lemurproject.galago.utility.tools.AppFunction;
import org.lemurproject.galago.utility.Parameters;
import org.lemurproject.galago.utility.tools.Arguments;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.*;
import java.util.logging.Logger;

/**
 *
 * @author sjh
 */
public class BatchSearch extends AppFunction {

  public static final Logger logger = Logger.getLogger("BatchSearch");

  public static void main(String[] args) throws Exception {
    (new BatchSearch()).run(Arguments.parse(args), System.out);
  }

  @Override
  public String getName() {
    return "batch-search";
  }

  @Override
  public String getHelpString() {
    return "galago simple-batch-search <args>\n\n"
            + "  Runs a batch of queries against an index and produces TREC-formatted\n"
            + "  output.  The output can be used with retrieval evaluation tools like\n"
            + "  galago eval (org.lemurproject.galago.core.eval).\n\n"
            + "  Sample invocation:\n"
            + "     galago batch-search --index=/tmp/myindex --requested=200 /tmp/queries.json \n\n"
            + "  Args:\n"
            + "     --index=path_to_your_index\n"
            + "     --requested=N               : Number of results to return for each query.  default=1000\n"
            + "     --operatorWrap=operator     : Wrap query text in the specified operator.\n"
            + "     --queryFormat=json|tsv      : Accept query file in JSON or TSV format.  default=json\n"

            + "     --showNoResults=true|false  : Print dummy result for queries with no results.\n"
            + "                                   This ensures query evaluation metrics account for queries\n"
            + "                                   that returned no results rather than skipping them.\n"
	    + "                                   Dummy doc will look like the following\n"
	    + "                                   <qid> Q0 no_results_found 1 -999.9 galago \n"
            + "                                   default=false\n"
            + "     --systemName=system_label   : A run label added to a results list queries.  Only available\n"
            + "                                   in trec mode (--trec=true).  default=galago\n"
	
            + "     /path/to/query/file.json    : Input file in xml parameters format (see below).\n\n"

	    + "  Query file format:\n"
            + "    The query file is an JSON file containing a set of queries.  Each query\n"
            + "    has text field, which contains the text of the query, and a number field, \n"
            + "    which uniquely identifies the query in the output.\n\n"
            + "  Example query file:\n"
            + "  {\n"
            + "     \"queries\" : [\n"
            + "       {\n"
            + "         \"number\" : \"CACM-408\", \n"
            + "         \"text\" : \"#combine(my query)\"\n"
            + "       },\n"
            + "       {\n"
            + "         \"number\" : \"WIKI-410\", \n"
            + "         \"text\" : \"#combine(another query)\" \n"
            + "       }\n"
            + "    ]\n"
            + "  }\n";
  }


    double GaussianCDF(double x, double mean, double sigma) {
        double res;
        x = (x - mean) / sigma;
        if (x == 0) {
            res = 0.5;
        } else {
            double oor2pi = 1 / (Math.sqrt(2.0 * Math.PI));
            double t = 1 / (1.0 + 0.2316419 * Math.abs(x));
            t *=
                    oor2pi * Math.exp(-0.5 * x * x)
                            * (0.31938153
                            + t
                            * (-0.356563782
                            + t
                            * (1.781477937
                            + t
                            * (-1.821255978
                            + t
                            * 1.330274429))));
            if (x >= 0) {
                res = 1.0 - t;
            } else {
                res = t;
            }
        }
        return res;
    }

    double KLDivergence(Map<NodeParameters, Integer[]> info , Map<NodeParameters, Double> plm) {
        double div = 0;
        for (NodeParameters qParameters : info.keySet()) {
            double pr = qParameters.getDouble("w");
            double doc_prob = plm.get(qParameters);
            div += pr * Math.log(pr / doc_prob);
        }

        return -div;
    }

    void DirSmoothing(Map<NodeParameters, Integer[]> info , Map<NodeParameters, Double> plm, double doc_len, double mu){
        double lambda = mu / (mu + doc_len);
        for (NodeParameters nodeParameters : info.keySet()) {
            double col_prob = ((double)(nodeParameters.getLong("nodeFrequency")))/ (double)(nodeParameters.getLong("collectionLength"));

            double doc_prob = plm.containsKey(nodeParameters) ?  ((1 - lambda) * plm.get(nodeParameters) + lambda * col_prob) : lambda * col_prob;

            plm.put(nodeParameters, doc_prob);
        }
    }

    double PropagationCount(double dis){
        return Math.exp(-dis * dis / 2);
    }

    double PropagationCountSum(int center, int doclen){
        double psg_len = Math.sqrt(2.0 * Math.PI) * 75 // #sigma
                * (GaussianCDF(doclen, (double) center, 75) //#sigma
                - GaussianCDF(0, (double) center, 75)); // #sigma

        return psg_len;
    }

    double calculatePLMScore(Map<NodeParameters, Integer[]> info, int doclen){
        double plmScore = -1000000;
        for (NodeParameters nodeParameters : info.keySet()) {
            for (Integer center : info.get(nodeParameters)) {

                double psg_len = PropagationCountSum(center, doclen);

                Map<NodeParameters, Double> plm = new HashMap<>();
                for (NodeParameters parameters : info.keySet()) {
                    for (Integer pos : info.get(parameters)) {
                        double pr = PropagationCount((double) (pos - center) / 75) / psg_len; // #sigma

                        if (pr > 0){
                            plm.putIfAbsent(parameters, 0.0);
                            plm.replace(parameters, plm.get(parameters) + pr);
                        }
                    }
                }

                DirSmoothing(info, plm, psg_len, 500); // #dirichletPrior
                double score = KLDivergence(info, plm);

                if (score > plmScore) {
                    plmScore = score;
                }
            }
        }

        return plmScore;
    }


    ScoredDocument[] runPLM(List<ScoredDocument> results, Map<Long, Map<NodeParameters, Integer[]>> info, int[] docLenghts){
        FixedSizeMinHeap<ScoredDocument> queue = new FixedSizeMinHeap<>(ScoredDocument.class, 1000, new ScoredDocument.ScoredDocumentComparator());
        for (ScoredDocument result : results) {
            double plmScore = calculatePLMScore(info.get(result.document), docLenghts[(int)result.document]);
            double score = 0.5 * plmScore + 0.5 * result.getScore(); // #coeff
            if (queue.size() < 1000 || queue.peek().score < score) {
                result.score = score;
                queue.offer(result);
            }
        }

        return ProcessingModel.toReversedArray(queue);
    }

    NodeParameters getqTermFromDirichlet(Node node){
        NodeParameters res = node.getNodeParameters();
        for (Node node1 : node) {
            if (node1.getOperator().equals("counts")){

                res.set("term", node1.getNodeParameters().getString("default"));
            }
        }


        return res;
    }

    List<NodeParameters> getQuery(Node transformed){
        List<NodeParameters> res = new ArrayList<>();
        for (Node node : transformed) {
            res.add(getqTermFromDirichlet(node));
        }

        return res;
    }


  @Override
  public void run(Parameters parameters, PrintStream out) throws Exception {
    List<ScoredDocument> results;
    if (!(parameters.containsKey("query") || parameters.containsKey("queries"))) {
      out.println(this.getHelpString());
      return;
    }

    // ensure we can print to a file instead of the commandline
    if (parameters.isString("outputFile")) {
      boolean append = parameters.get("appendFile", false);
      out = new PrintStream(new BufferedOutputStream(
              new FileOutputStream(parameters.getString("outputFile"), append)), true, "UTF-8");
    }

    //- Do we show a no result query dummy doc in output?
    boolean showNoResults = false;
    if (parameters.containsKey ("showNoResults")) {
      showNoResults = parameters.getBoolean ("showNoResults");
    }

    //- Set a system name for the query submissions
    String sysName = parameters.get ("systemName", "galago");
    
    // get queries
    List<Parameters> queries;
    String queryFormat = parameters.get("queryFormat", "json").toLowerCase();
    switch (queryFormat)
    {
      case "json":
        queries = JSONQueryFormat.collectQueries(parameters);
        break;
      case "tsv":
        queries = JSONQueryFormat.collectTSVQueries(parameters);
        break;
      default: throw new IllegalArgumentException("Unknown queryFormat: "+queryFormat+" try one of JSON, TSV");
    }

    // open index
    LocalRetrieval retrieval = (LocalRetrieval) RetrievalFactory.create(parameters);

    // record results requested
    int requested = (int) parameters.get("requested", 1000);

    // for each query, run it, get the results, print in TREC format

    for (Parameters query : queries) {
      String queryText = query.getString("text");
      String queryNumber = query.getString("number");

      query.setBackoff(parameters);
      query.set("requested", requested);

      // option to fold query cases -- note that some parameters may require upper case
      if (query.get("casefold", false)) {
        queryText = queryText.toLowerCase();
      }

      if (parameters.get("verbose", false)) {
        logger.info("RUNNING: " + queryNumber + " : " + queryText);
      }

      // parse and transform query into runnable form
      Node root = StructuredQuery.parse(queryText);

      // --operatorWrap=sdm will now #sdm(...text... here)
      if(parameters.isString("operatorWrap")) {
        if(root.getOperator().equals("root")) {
          root.setOperator(parameters.getString("operatorWrap"));
        } else {
          Node oldRoot = root;
          root = new Node(parameters.getString("operatorWrap"));
          root.add(oldRoot);
        }
      }
      Node transformed = retrieval.transformQuery(root, query);
      if (parameters.get("verbose", false)) {
        logger.info("Transformed Query:\n" + transformed.toPrettyString());
      }

      // run query
      results = retrieval.executeQuery(transformed, query).scoredDocuments;


        File pathPosting = new File( query.getString("index") + "postings.krovetz");
        DiskIndex index = new DiskIndex( query.getString("index") );
        IndexPartReader posting = DiskIndex.openIndexPart(pathPosting.getAbsolutePath());
        query.set("stemmer", "org.lemurproject.galago.core.parse.stem.KrovetzStemmer");
        Stemmer stemmer = Stemmer.create(query);

        KeyIterator vocabulary = posting.getIterator();

        Map<Long, Map<NodeParameters, Integer[]>> info = new HashMap<>(); // {docId : {qTermParams : positions_in_doc}

        int doc_lengths[] = new int[2000000];
        for(int i =0; i< 2000000; i++)
            doc_lengths[i] = -1;
        if (!results.isEmpty()) {
            for (ScoredDocument sd : results) {
                doc_lengths[(int)sd.document] = index.getLength(sd.document);
            }
        }

        PassageScoringContext sc = new PassageScoringContext();

        if (!results.isEmpty()) {

            List<NodeParameters> queryTerms = getQuery(transformed);

            for (NodeParameters queryTerm : queryTerms) {


                    for (ScoredDocument scoredDocument : results) {

                        BaseIterator iterator =  index.getIterator(
                                StructuredQuery.parse("#extents:" + queryTerm.getString("term") + ":part=postings.krovetz()"));

                        try{
                            DiskExtentIterator extentIterator = (DiskExtentIterator) iterator;
//                vocabulary.skipToKey( ByteUtil.fromString( stemmer.stemAsRequired(queryTerm.getString("term")) ) );
//                if ( stemmer.stemAsRequired(queryTerm.getString("term")).equals(vocabulary.getKeyString() ) ) {
//                        ExtentIterator extentIterator = (ExtentIterator) vocabulary.getValueIterator();


                            sc.document = scoredDocument.document;
                            extentIterator.syncTo(scoredDocument.document);


//                    ScoringContext sc = new ScoringContext();

//                        while ( !extentIterator.isDone() ) {


//                            sc.document = extentIterator.currentCandidate();
//                    if (doc_lengths[(int) sc.document] > 0) {

                            int freq = extentIterator.count(sc);
                            ExtentArray extents = extentIterator.extents(sc);

                            info.putIfAbsent(sc.document, new HashMap<>());
                            Integer poses[] = new Integer[extents.size()];
//                            System.out.print(queryTerm.getString("term") + ", doc: " + sc.document + ", cout: " + freq + "poses: ");
                            for (int i = 0; i < extents.size(); i++) {
//                                System.out.print((i > 0 ? "," : "") + extents.begin(i));
                                poses[i] = extents.begin(i);
                            }

//                            System.out.println();
                            info.get(sc.document).put(queryTerm, poses);
                        } catch (Exception e){
                            System.out.println("could not create extent iterator for: " + queryTerm.getString("term"));
                        }

                    }
//                    }
//                            extentIterator.movePast( extentIterator.currentCandidate() );
//                        }


//                } else{
//                    System.out.println("term not found: " + queryTerm.getString("term"));
//                }
                }
//        } else{
//            System.out.println("no result found for query");
//        }
        }
        ScoredDocument[] finRes = runPLM(results, info, doc_lengths);

      // if we have some results -- print in to output stream
      boolean trecFmt = query.get("trec", false);

      if (!results.isEmpty()) {
        for (ScoredDocument sd : finRes) {
          if (trecFmt) {
            //out.println(sd.toTRECformat(queryNumber));
            out.println (sd.toTRECformat (queryNumber, sysName));
          } else {
            //out.println(sd.toString(queryNumber));
            out.println (sd.toString (queryNumber));
          }
        }
      }
      // Even if no results, print SOMETHING so we know.  Evaluation metrics
      // get thrown off when a query is unaccounted for in a ranked list because
      // nothing was retrieved.  Print dummy document output.
      else {
        if (showNoResults) {
          ScoredDocument sd = new ScoredDocument ();
          sd.score = -999;
          sd.rank = 1;
          sd.documentName = "no_results_found";
	
          if (trecFmt) {
            //out.printf ("%s Q0 no_results_found 1 -999 %s\n", queryNumber, sysName);
            out.println (sd.toTRECformat (queryNumber, sysName));
          }
          else {
	    //out.printf ("%s Q0 no_results_found 1 -999 galago\n", queryNumber, sysName);
            out.println (sd.toString (queryNumber));
          }
        }
      }
    }

    if (parameters.isString("outputFile")) {
      out.close();
    }
  }

}
